module.exports = {
  siteMetadata: {
    pathPrefix: `/personal-site`,
    title: 'Jame Coyne',
    description:
      'Jame Coyne does things (usually with computers) around Boston, MA',
    url: 'https://gatsby-starter-amsterdam.netlify.com',
    author: 'Jame Coyne',
    image: 'https://gatsby-starter-amsterdam.netlify.com/og-image.jpg',
    intro: 'Jame Coyne does things with computers in Boston, MA.',
    menuLinks: [
      {
        name: 'Projects',
        slug: '/',
      },
      {
        name: 'About',
        slug: '/about/',
      },
    ],
    footerLinks: [
      {
        name: 'Linkedin',
        url: 'https://www.linkedin.com/in/jame-coyne/',
      },
    ],
  },
  plugins: [
    {
      resolve: 'gatsby-theme-amsterdam',
      options: {},
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Gatsby Theme Amsterdam`,
        short_name: `Amsterdam`,
        background_color: `#f5f0eb`,
        theme_color: `#f5f0eb`,
        start_url: `/about`,
        display: `standalone`,
        icon: require.resolve('./src/images/favicon.png'),
      },
    },
  ],
}
